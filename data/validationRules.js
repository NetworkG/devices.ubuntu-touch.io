const fs = require("fs");
const path = require('path');

const portStatus = require("./portStatus.json");
const deviceInfo = require("./deviceInfo.json");

module.exports = function() {
  let rules = {
    "codename": ["required", "string"],
    "name": ["required", "string"],
    "deviceType": ["required", "string", "in:phone,tablet,tv,other"],
    "portType": ["required", "string", "in:Legacy,Native,Halium 5.1,Halium 7.1,Halium 9.0,Halium 10.0"],
    "description": ["required", "string"],
    "price": ["required", "integer", "between:10,2000"],
    "subforum": ["required", "string"],
    "installLink": "url",
    "buyLink": "url", // TODO: handle disabled buy button
    "tag": "in:promoted,unmaintained",
    "image": "url",
    "aliases.*": ["string", "alpha_dash", "different:fileInfo.name"],

// Port status validation
    "portStatus": ["required", "array"],
    "portStatus.*": [],
    "portStatus.*.features": ["required", "array"],
    "portStatus.*.features.*.id": ["required", "string", "alpha"],
    "portStatus.*.features.*.value": ["required", "string", "in:+,-,+-,?,x"],
    "maturity": ["required_without:portStatus", "numeric"],

// Device specifications validation
    "deviceInfo": ["required"],
    "deviceInfo.*.id": ["required", "string", "alpha"],
    "deviceInfo.*.value": ["required", "string"],

// Contributors validation
    "contributors": ["required", "array", "between:1,50"],
    "contributors.*.name": ["required", "string", "between:1,20"],
    "contributors.*.forum": ["present", "url"],
    "contributors.*.photo": ["present", "url"],

// Community help validation
    "communityHelp": ["required", "array", "between:1,20"],
    "communityHelp.*.name": ["required", "string", "between:1,50"],
    "communityHelp.*.link": ["required", "url"],
    "communityHelp.*.icon": ["required"],

// Documentation links validation
    "docLinks": ["array", "between:1,20"],
    "docLinks.*.name": ["required", "string", "between:1,50"],
    "docLinks.*.link": ["required", "url"],

// External links validation
    "externalLinks": ["required", "array", "between:1,20"],
    "externalLinks.*.name": ["required", "string", "between:1,50"],
    "externalLinks.*.link": ["required", "url"],
    "externalLinks.*.icon": ["required"],

// Seo data validation
    "seo.description": "string",
    "seo.keywords": "string",

// Installer support, installLink and instructions content
    "installLink": ["url"],
    "enableMdRendering": ["boolean"],
    "content": ["required_if:enableMdRendering,true", "string"],
  };

// Get rules from deviceInfo.json
  deviceInfo.forEach((el) => {
    rules.deviceInfo.push({"contains": ["id", el.id]});
  });

// Get rules from portStatus.json
  for (const key in portStatus) {
    rules.portStatus.push({"contains": ["categoryName", key]});
    portStatus[key].forEach((el) => {
      rules["portStatus.*"].push({"contains_if": ["id", el.id, "features", "categoryName", key]});
    });
  }

// List of icons in /src/assets/img/services
  let extLinkIcons = {"in": []}
  fs.readdirSync(path.join(__dirname, "../src/assets/img/services"))
    .filter(el => path.extname(el) === '.svg')
    .forEach((el) => extLinkIcons.in.push(path.parse(el).name));
  rules["externalLinks.*.icon"].push(extLinkIcons);
  rules["communityHelp.*.icon"].push(extLinkIcons);

  return rules;
};
